/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAME_H
#define GAME_H

#include "mastercontrol.h"

enum GameStatus{ GS_MAIN, GS_PLAY, GS_PAUSED, GS_MODAL };

class KOCam;
class Dungeon;
class Character;
class Player;

typedef struct World
{
    SharedPtr<KOCam> camera;
    SharedPtr<Dungeon> dungeon_;
    SharedPtr<Character> ko;
    struct {
        SharedPtr<Node> sceneCursor;
        SharedPtr<Cursor> uiCursor;
        PODVector<RayQueryResult> hitResults;
    } cursor;
} World;


class Game: public Object
{
    DRY_OBJECT(Game, Object);

public:
    Game(Context* context);

    GameStatus GetStatus() const { return status_; }
    void SetStatus(GameStatus status) { status_ = status; }

    void StartNew();
    void TogglePause();

    Scene* GetScene() const { return scene_.Get(); }
    Vector< SharedPtr<Player> > GetPlayers() const;
    Player* GetPlayer(int playerID) const;

    World world_;

private:
    void CreateScene();

    SharedPtr<Scene> scene_;
    GameStatus status_;
    Vector< SharedPtr<Player> > players_;
};

#endif // GAME_H
