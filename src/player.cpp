/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "player.h"

#include "inputmaster.h"

Player::Player(int playerId, Context* context): Object(context),
    playerId_{ playerId },
    human_{},
    alive_{ true }
{
}

void Player::Die()
{
    alive_ = false;
}

void Player::Respawn()
{
    alive_ = true;
}

Vector3 Player::GetPosition()
{
    return GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_)->GetPosition();
}

Controllable* Player::GetControllable()
{
    Controllable* controllable{ GetSubsystem<InputMaster>()->GetControllableByPlayer(playerId_) };

    if (controllable)
        return controllable;
    else
        return nullptr;
}
