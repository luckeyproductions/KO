/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "networkmaster.h"
#include "inputmaster.h"
#include "settings.h"
#include "kocam.h"
#include "ui/gui.h"
#include "game.h"
#include "materials.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context): Application(context),
    paused_{ false },
    drawDebug_{ false }
{
}

void MasterControl::Setup()
{
    engineParameters_[EP_WINDOW_TITLE]  = "KO & the Curse of Greyface";
    engineParameters_[EP_LOG_NAME]      = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "ko.log";
    engineParameters_[EP_WINDOW_ICON]   = "icon.png";
    engineParameters_[EP_VSYNC]         = true;

    //Add resource paths
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    const String resourcePaths{ (fs->DirExists(fs->GetProgramDir() + "Resources") ? "Resources"
                               : fs->GetAppPreferencesDir("luckey", "ko")) };

    engineParameters_[EP_RESOURCE_PATHS] = resourcePaths;

    Settings* settings{ context_->RegisterSubsystem<Settings>() };
    if (settings->LoadSettings())
    {
        engineParameters_[EP_WINDOW_WIDTH]  = settings->GetWindowWidth();
        engineParameters_[EP_WINDOW_HEIGHT] = settings->GetWindowHeight();
        engineParameters_[EP_FULL_SCREEN]   = settings->GetFullscreen();
        engineParameters_[EP_VSYNC]         = settings->GetVSync();
    }
}

void MasterControl::Start()
{
    SetRandomSeed(GetSubsystem<Time>()->GetSystemTime());
    GetSubsystem<Network>()->SetUpdateFps(60);

    KOCam::RegisterObject(context_);

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem(new InputMaster{ context_ });
    context_->RegisterSubsystem<Script>();

//    CreateConsoleAndDebugHud();
    context_->RegisterSubsystem<Materials>();
    context_->RegisterSubsystem<Game>();
    context_->RegisterSubsystem<GUI>();

//    context_->RegisterSubsystem(new NetworkMaster(context_));

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(MasterControl, DrawDebug));
}

void MasterControl::Stop()
{
    GetSubsystem<Settings>()->SaveSettings();

    engine_->DumpResources(true);
}

void MasterControl::CreateConsoleAndDebugHud()
{
    XMLFile* defaultStyle{ RES(XMLFile, "UI/DefaultStyle.xml") };

    // Create console
    Console* console{ engine_->CreateConsole() };
    console->SetDefaultStyle(defaultStyle);
    console->GetBackground()->SetOpacity(.8f);

    // Create debug HUD.
    DebugHud* debugHud{ engine_->CreateDebugHud() };
    debugHud->SetDefaultStyle(defaultStyle);

//    debugHud->SetMode(DEBUGHUD_SHOW_ALL);
}

float MasterControl::Sine(float freq, float min, float max, float shift)
{
    float phase{ freq * GetSubsystem<Game>()->GetScene()->GetElapsedTime() + shift };
    float add{ .5f * (min + max) };
    return LucKey::Sine(phase) * .5f * (max - min) + add;
}

void MasterControl::DrawDebug(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Input* input{ GetSubsystem<Input>() };

    if (input->GetKeyPress(KEY_0))
        drawDebug_ = !drawDebug_;

    if (drawDebug_)
        GetSubsystem<Game>()->GetScene()->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
}
