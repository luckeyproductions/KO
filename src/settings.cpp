/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "settings.h"

Settings::Settings(Context* context): Object(context),
    windowWidth_{},
    windowHeight_{},
    fullscreen_{ true },
    vsync_{ true },
    antiAliasing_{ true },
    bloom_{ true },
    musicEnabled_{ true },
    musicGain_{ 1.f }
{
}

bool Settings::LoadSettings()
{
    /// \\ Should use FILES->GetAppPreferencesDir("luckey", "ko")
    FileSystem* fs{ GetSubsystem<FileSystem>() };

    if (fs->FileExists("Resources/Settings.xml"))
    {
        File file{ context_, "Resources/Settings.xml", FILE_READ };
        XMLFile configFile{ context_ };
        configFile.Load(file);
        const XMLElement graphicsSettings{ configFile.GetRoot().GetChild("Graphics") };
        const XMLElement audioSettings{ configFile.GetRoot().GetChild("Audio") };

        if (graphicsSettings)
        {
            windowWidth_  = graphicsSettings.GetInt("Width");
            windowHeight_ = graphicsSettings.GetInt("Height");
            fullscreen_   = graphicsSettings.GetBool("Fullscreen");
            vsync_        = graphicsSettings.GetBool("VSync");
            antiAliasing_ = graphicsSettings.GetBool("AntiAliasing");
            bloom_        = graphicsSettings.GetBool("Bloom");
        }
        if (audioSettings)
        {
            musicEnabled_ = audioSettings.GetBool("MusicEnabled");
            musicGain_    = audioSettings.GetFloat("MusicGain");

            GetSubsystem<Audio>()->SetMasterGain(SOUND_MUSIC, musicEnabled_ * musicGain_);
        }

        return true;
    }
    else
    {
        return false;
    }
}

void Settings::SaveSettings()
{
    XMLFile file{ context_ };
    XMLElement root{ file.CreateRoot("Settings") };

    XMLElement graphicsElement{ root.CreateChild("Graphics") };
    graphicsElement.SetInt("Width", windowWidth_);
    graphicsElement.SetInt("Height", windowHeight_);
    graphicsElement.SetBool("Fullscreen", fullscreen_);
    graphicsElement.SetBool("VSync", vsync_);
    graphicsElement.SetBool("AntiAliasing", antiAliasing_);
    graphicsElement.SetBool("Bloom", bloom_);

    XMLElement audioElement(root.CreateChild("Audio"));
    audioElement.SetFloat("MusicEnabled", musicEnabled_);
    audioElement.SetFloat("MusicGain", musicGain_);

    file.SaveFile("Resources/Settings.xml");
}

void Settings::ToggleFullscreen()
{
    GetSubsystem<Graphics>()->ToggleFullscreen();
    fullscreen_ = !fullscreen_;
}

void Settings::ToggleAntiAliasing()
{
    Renderer* renderer{ GetSubsystem<Renderer>() };

    antiAliasing_ = !antiAliasing_;

    for (unsigned v{ 0 }; v < renderer->GetNumViewports(); ++v)
    {
        Viewport* viewport{ renderer->GetViewport(v) };
        viewport->GetRenderPath()->SetEnabled("FXAA3", antiAliasing_);
    }
}

void Settings::ToggleVSync()
{
    Graphics* g{ GetSubsystem<Graphics>() };

    vsync_ = !vsync_;

    g->SetMode(g->GetWidth(), g->GetHeight(), fullscreen_, g->GetBorderless(), g->GetResizable(), g->GetHighDPI(),
               vsync_,
               g->GetTripleBuffer(), g->GetMultiSample(), g->GetMonitor(), g->GetRefreshRate());
}

void Settings::ToggleBloom()
{
    Renderer* renderer{ GetSubsystem<Renderer>() };

    bloom_ = !bloom_;

    for (unsigned v{ 0 }; v < renderer->GetNumViewports(); ++v)
    {
        Viewport* viewport{ renderer->GetViewport(v) };
        viewport->GetRenderPath()->SetEnabled("BloomHDR", bloom_);
    }
}

void Settings::SetMusicEnabled(bool enabled)
{
    musicEnabled_ = enabled;

    GetSubsystem<Audio>()->SetMasterGain(SOUND_MUSIC, musicEnabled_ * musicGain_);
}

void Settings::SetMusicGain(float gain)
{
    musicGain_ = gain;

    GetSubsystem<Audio>()->SetMasterGain(SOUND_MUSIC, musicEnabled_ * musicGain_);
}
