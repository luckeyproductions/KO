/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include "mastercontrol.h"

#include "materials.h"

class SceneObject: public LogicComponent
{
    DRY_OBJECT(SceneObject, LogicComponent);

public:
    SceneObject(Context *context);
    void DelayedStart() override;

    void Set(const Vector3& position);
    Vector3 GetPosition() const {return node_->GetWorldPosition();}

    void PlaySound(Sound* sample);

protected:
    void Disable();
    const float variator_;

};

#endif // SCENEOBJECT_H
