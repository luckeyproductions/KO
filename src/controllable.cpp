/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "controllable.h"

#include "inputmaster.h"
#include "player.h"
#include "kocam.h"

Controllable::Controllable(Context* context): SceneObject(context),
    controlled_{false},
    move_{},
    aim_{},
    maxPitch_{ 90.0f },
    minPitch_{ 0.0f },
    actions_{},
    actionSince_{},
    model_{ nullptr },
    rigidBody_{ nullptr },
    collisionShape_{ nullptr },
    animCtrl_{ nullptr }
{
    for (int a{ 0 }; a < 4; ++a)
        actionSince_[a] = 0.0f;
}

void Controllable::Start()
{
    model_ = node_->CreateComponent<AnimatedModel>(REPLICATED);
    model_->SetCastShadows(true);

    animCtrl_ = node_->CreateComponent<AnimationController>(REPLICATED);

    rigidBody_ = node_->CreateComponent<RigidBody>(REPLICATED);
    rigidBody_->SetKinematic(true);
    rigidBody_->SetTrigger(true);
    rigidBody_->SetCollisionEventMode(COLLISION_ALWAYS);

    collisionShape_ = node_->CreateComponent<CollisionShape>(REPLICATED);
}
void Controllable::Update(float timeStep)
{
    for (int a{ 0 }; a < static_cast<int>(actions_.size()); ++a)
    {
        if (actions_[a] || actionSince_[a] > 0.0f)
            actionSince_[a] += timeStep;
    }

    if (GetPlayer() && GetPlayer()->IsHuman())
    {
    }
    else
    {
        Think();
    }
}

void Controllable::SetMove(Vector3 move)
{
    if (move.Length() > 1.0f)
        move.Normalize();

    Player* player{ GetPlayer() };

    if (player && player->GetCamera())
        move = Quaternion(player->GetCamera()->GetRotation().YawAngle(), Vector3::UP) * move;

    move_ = move;
}

void Controllable::SetAim(Vector3 aim)
{
    aim_ = aim.Normalized();
}

void Controllable::SetActions(std::bitset<4> actions)
{
    if (actions == actions_)
    {
        return;
    }
    else
    {
        for (int i{ 0 }; i < static_cast<int>(actions.size()); ++i) {

            if (actions[i] != actions_[i])
            {
                actions_[i] = actions[i];

                if (actions[i])
                    HandleAction(i);
            }
        }
    }
}

void Controllable::AlignWithMovement(float timeStep, Quaternion offset)
{
    Quaternion rot{ node_->GetRotation() };
    Quaternion targetRot{};
    targetRot.FromLookRotation(offset * move_);
    rot = rot.Slerp(targetRot, Clamp(timeStep * 5.0f, 0.0f, 1.0f));

    node_->SetRotation(rot);
}

void Controllable::ClampPitch(Quaternion& rot)
{
    const float maxCorrection{ rot.EulerAngles().x_ - maxPitch_ };
    if (maxCorrection > 0.0f)
        rot = Quaternion(-maxCorrection, node_->GetRight()) * rot;

    const float minCorrection{ rot.EulerAngles().x_ - minPitch_ };
    if (minCorrection < 0.0f)
        rot = Quaternion(-minCorrection, node_->GetRight()) * rot;
}

void Controllable::ClearControl()
{
    ResetInput();
}

Player* Controllable::GetPlayer()
{
    return INPUTMASTER->GetPlayerByControllable(this);
}
