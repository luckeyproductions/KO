
#ifndef PORTAL_H
#define PORTAL_H

#include "../mastercontrol.h"


class Portal: public Component
{
    DRY_OBJECT(Portal, Component);

public:
    Portal(Context* context);
    void OnSetEnabled() override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    void HandlePortalCollision(StringHash eventType, VariantMap& eventData);

    Node* sideA_;
    Node* sideB_;
};

#endif // PORTAL_H
