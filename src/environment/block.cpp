#include "block.h"
#include "../mastercontrol.h"

HashMap<unsigned, StaticModelGroup*> Block::blockGroups_{};

void Block::RegisterObject(Context* context)
{
    context->RegisterFactory<Block>();
}

Block::Block(Context* context): Component(context)
{
}

void Block::Initialize(Model* model, Material* material, Model* collisionMesh)
{
    assert(GetScene());

    if (model == nullptr && collisionMesh == nullptr)
    {
        Remove();
        return;
    }

    if (collisionMesh)
    {
        RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
        rigidBody->SetCollisionLayer(0);
        rigidBody->SetFriction(0.8f);
        collider_ = node_->CreateComponent<CollisionShape>();
        collider_->SetTriangleMesh(model);
    }

    unsigned blockHash{ model->GetNameHash().ToHash() ^ material->GetNameHash().ToHash() };

    if (!blockGroups_.Contains(blockHash))
    {
        blockGroups_[blockHash] = GetScene()->CreateComponent<StaticModelGroup>();
        blockGroups_[blockHash]->SetModel(model);
        blockGroups_[blockHash]->SetCastShadows(true);
        blockGroups_[blockHash]->SetMaterial(material);
    }

    blockGroups_[blockHash]->AddInstanceNode(node_);
}
