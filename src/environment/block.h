#ifndef BLOCK_H
#define BLOCK_H

#include "../luckey.h"

class Block : public Component
{
    DRY_OBJECT(Block, Component);

public:
    static void RegisterObject(Context* context);
    static const HashMap<unsigned, StaticModelGroup*>& Groups() { return blockGroups_; }

    Block(Context* context);

    void Initialize(Model* model = nullptr, Material* material = nullptr, Model* collisionMesh = nullptr);

private:
    static HashMap<unsigned, StaticModelGroup*> blockGroups_;
    CollisionShape* collider_;
};

#endif // BLOCK_H
