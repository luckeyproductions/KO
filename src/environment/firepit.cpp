/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "firepit.h"

void FirePit::RegisterObject(Context *context)
{
    context->RegisterFactory<FirePit>();
}

FirePit::FirePit(Context* context):
    SceneObject(context),
    lightNode_{},
    light_{}
{
}

void FirePit::OnNodeSet(Node *node)
{ if (!node) return;

    node_->SetRotation(Quaternion(0.0f, variator_ * 360.0f, 0.0f));

    StaticModel* model_{ node_->CreateComponent<StaticModel>() };
    model_->SetModel(RES(Model, "Models/FirePit.mdl"));
    model_->SetMaterial(0, RES(Material, "Materials/Metal.xml"));
    model_->SetMaterial(1, RES(Material, "Materials/Darkness.xml"));
    model_->SetMaterial(2, RES(Material, "Materials/Amber.xml"));
    model_->SetCastShadows(true);

    RigidBody* rigidBody{ node_->CreateComponent<RigidBody>() };
//    rigidBody->SetMass(1.0f);
//    rigidBody->SetLinearFactor(Vector3::ONE - Vector3::UP);
    CollisionShape* collider{ node_->CreateComponent<CollisionShape>() };
    collider->SetCylinder(0.4f, 0.5f);

    Node* particleNode{ node_->CreateChild("Fire", LOCAL) };
    particleNode->SetPosition(Vector3::UP * 0.16f);
    ParticleEmitter* flameEmitter{ particleNode->CreateComponent<ParticleEmitter>() };
    flameEmitter->SetEffect(RES(ParticleEffect, "Particles/fire1.xml"));
    ParticleEmitter* sparkEmitter{ particleNode->CreateComponent<ParticleEmitter>() };
    sparkEmitter->SetEffect(RES(ParticleEffect, "Particles/fire_sparks.xml"));

    lightNode_ = node_->CreateChild("LightNode", LOCAL);
    light_ = lightNode_->CreateComponent<Light>();
    light_->SetColor(Color(1.0f, 0.6f, 0.4f));
    light_->SetRange(5.0f);
    light_->SetCastShadows(true);
    light_->SetShadowResolution(0.25f);
}

void FirePit::Update(float timeStep)
{ (void)timeStep;

    if (lightNode_) {
        UpdateLightPosition();
        UpdateBrightness();
    }
}

void FirePit::UpdateLightPosition()
{
    float range{ 0.001f };

    float x{ 0.0f };
    float y{ 0.5f };
    float z{ 0.0f };

    for (int i{1}; i < 9; ++i)
        x += GetSubsystem<MasterControl>()->Sine(4.0f + i, -range, range, i + (i * variator_ * 1.0f * M_PI)) / (i * 0.666f);
    for (int i{1}; i < 9; ++i)
        y += GetSubsystem<MasterControl>()->Sine(5.0f + i, -range, range, i + (i * variator_ * 1.5f * M_PI)) / (i * 0.666f);
    for (int i{1}; i < 9; ++i)
        z += GetSubsystem<MasterControl>()->Sine(6.0f + i, -range, range, i + (i * variator_ * 2.0f * M_PI)) / (i * 0.666f);

    lightNode_->SetPosition(Vector3(x, y, z));
}

void FirePit::UpdateBrightness()
{
    float brightness{ 1.23f };
    for (int i{1}; i < 5; ++i)
    {
        brightness += GetSubsystem<MasterControl>()->Sine(variator_ + 7.123f + i, -0.001f, 0.023f, (variator_ * 2.0f*M_PI) + i*(0.2f+variator_));
    }
    light_->SetBrightness(brightness);
}
