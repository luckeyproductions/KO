/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed z the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../mastercontrol.h"
#include "../game.h"

#include "../characters/floatingeye.h"
#include "../characters/character.h"
#include "../kocam.h"
#include "firepit.h"
#include "frop.h"
#include "block.h"

#include "dungeon.h"

Dungeon::Dungeon(Context* context): Component(context),
    rigidBody_{ nullptr },
    groundCollider_{ nullptr }
{
}

void Dungeon::OnNodeSet(Node* node)
{
    if (!node)
        return;

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetFriction(2.0f);

    groundCollider_ = node_->CreateComponent<CollisionShape>();

//    LoadBlockMap(CACHE->GetResource<XMLFile>("Maps/Test.emp"));
}

void Dungeon::LoadBlockMap(XMLFile* blockMap)
{
    assert(blockMap);

    const XMLElement mapElem{ blockMap->GetRoot("blockmap") };
    const IntVector3 mapSize{ mapElem.GetIntVector3("map_size") };
    const Vector3 blockSize{ mapElem.GetVector3("block_size") };
    HashMap<int, HashMap<int, Pair<String, String> > > blockSets{};
    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };

    groundCollider_->SetStaticPlane(blockSize.y_ * (mapSize.y_ - mapSize.y_ % 2) * Vector3::DOWN * .5f);

    // Load blocksets
    while (blockSetRefElem)
    {
        unsigned blockSetId{ blockSetRefElem.GetUInt("id") };

        XMLFile* blockSetXML{ CACHE->GetResource<XMLFile>("Maps/" + blockSetRefElem.GetAttribute("name")) };
        XMLElement blockSetElem{ blockSetXML->GetRoot("blockset") };
        XMLElement blockElem{ blockSetElem.GetChild("block") };

        while (blockElem)
        {
            blockSets[blockSetId][blockElem.GetUInt("id")].first_  = blockElem.GetAttribute("model");
            blockSets[blockSetId][blockElem.GetUInt("id")].second_ = blockElem.HasAttribute("material") ? blockElem.GetAttribute("material")
                                                                                                        : blockElem.GetChild("material").GetAttribute("name");

            blockElem = blockElem.GetNext("block");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    XMLElement layerElem{ mapElem.GetChild("gridlayer") };

    while (layerElem)
    {
        XMLElement blockElem{ layerElem.GetChild("gridblock") };

        while (blockElem)
        {
            Node* blockNode{ node_->CreateChild("Block") };
            blockNode->SetRotation(blockElem.GetQuaternion("rot"));
            blockNode->SetPosition((blockElem.GetVector3("coords") - mapSize * 0.5f) * blockSize);

            const String modelName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].first_ };
            const String materialName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].second_ };

            Block* block{ blockNode->CreateComponent<Block>() };
            block->Initialize(CACHE->GetResource<Model>(modelName),
                              CACHE->GetResource<Material>(materialName));

            blockElem = blockElem.GetNext("gridblock");
        }

        layerElem = layerElem.GetNext("gridlayer");
    }
}
