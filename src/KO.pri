SOURCES += \
    $$PWD/characters/character.cpp \
    $$PWD/characters/floatingeye.cpp \
    $$PWD/characters/human.cpp \
    $$PWD/characters/npc.cpp \
    $$PWD/controllable.cpp \
    $$PWD/environment/block.cpp \
    $$PWD/environment/deco.cpp \
    $$PWD/environment/dungeon.cpp \
    $$PWD/environment/firepit.cpp \
    $$PWD/environment/frop.cpp \
    $$PWD/environment/portal.cpp \
    $$PWD/ui/gui.cpp \
    $$PWD/ui/overlay.cpp \
    $$PWD/ui/tome.cpp \
    $$PWD/ui/tomepages.cpp \
    $$PWD/equipment.cpp \
    $$PWD/game.cpp \
    $$PWD/inputmaster.cpp \
    $$PWD/item.cpp \
    $$PWD/jukebox.cpp \
    $$PWD/kocam.cpp \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/materials.cpp \
    $$PWD/networkmaster.cpp \
    $$PWD/player.cpp \
    $$PWD/sceneobject.cpp \
    $$PWD/settings.cpp

HEADERS += \
    $$PWD/characters/character.h \
    $$PWD/characters/floatingeye.h \
    $$PWD/characters/human.h \
    $$PWD/characters/npc.h \
    $$PWD/controllable.h \
    $$PWD/environment/block.h \
    $$PWD/environment/deco.h \
    $$PWD/environment/dungeon.h \
    $$PWD/environment/firepit.h \
    $$PWD/environment/frop.h \
    $$PWD/environment/portal.h \
    $$PWD/ui/gui.h \
    $$PWD/ui/overlay.h \
    $$PWD/ui/tome.h \
    $$PWD/equipment.h \
    $$PWD/game.h \
    $$PWD/inputmaster.h \
    $$PWD/item.h \
    $$PWD/jukebox.h \
    $$PWD/kocam.h \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/materials.h \
    $$PWD/networkmaster.h \
    $$PWD/player.h \
    $$PWD/sceneobject.h \
    $$PWD/settings.h
