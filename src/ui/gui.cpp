/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gui.h"

GUI::GUI(Context* context): Object(context),
    overlay_{ nullptr }
{
    Overlay::RegisterObject(context_);

    CreateUI();
    CreateCursor();
}

void GUI::CreateUI()
{
    UIElement* uiRoot{ GetSubsystem<UI>()->GetRoot() };
    uiRoot->SetDefaultStyle(RES(XMLFile, "UI/DefaultStyle.xml"));

    overlay_ = MakeShared<Overlay>(context_);

    CreateCursor();
}

void GUI::CreateCursor()
{
    IntVector2 resolution{ GetSubsystem<Graphics>()->GetSize() };
    Pair<String, int> cursor{ "", 96 };
    if (resolution.y_ >= 1080)
        cursor = { "_big", 128 };
    else if (resolution.y_ <= 720)
        cursor = { "_small", 64 };

    const int cursorWidth{ cursor.second_ };
    IntRect cursorSize{ 0, 0, cursorWidth, cursorWidth * 2 };


    UI* ui{ GetSubsystem<UI>() };
    UIElement* root{ ui->GetRoot() };

    Cursor* c{ root->CreateChild<Cursor>() };
    c->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    c->DefineShape(CS_NORMAL, RES(Image, String{ "UI/Cursor" } + cursor.first_ + ".png"), cursorSize, { 1, 1 });
    ui->SetCursor(c);

    c->SetPosition(2 * resolution / 3);
    c->SetBlendMode(BLEND_ALPHA);
}
