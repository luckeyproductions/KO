/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../settings.h"
#include "overlay.h"

void Overlay::RegisterObject(Context* context)
{
    context->RegisterFactory<Overlay>();
    context->RegisterFactory<Tome>();
}

Overlay::Overlay(Context* context): Object(context),
    cameraNode_{ nullptr },
    tableNode_{ nullptr }
{
    CreateScene();

    Node* tomeNode{ overlayScene_->CreateChild("Tome") };
    tomeNode->SetPosition(tableNode_->GetPosition() + Vector3::BACK * .17f);
    tome_ = tomeNode->CreateComponent<Tome>();
}

void Overlay::CreateScene()
{
    overlayScene_ = MakeShared<Scene>(context_);
    overlayScene_->CreateComponent<Octree>();
    Zone* zone{ overlayScene_->CreateComponent<Zone>() };
    zone->SetAmbientColor(Color::BLACK.Lerp(Color::WHITE, .42f));

    tableNode_ = overlayScene_->CreateChild("Table");
    tableNode_->Translate(Vector3::DOWN);
    tableNode_->SetScale({ 2.f, 1.f, 1.f });
    StaticModel* table{ tableNode_->CreateComponent<StaticModel>() };
    table->SetModel(RES(Model, "Models/Plane.mdl"));
    table->SetMaterial(RES(Material, "Materials/Planks.xml"));

    Node* lightNode{ overlayScene_->CreateChild("DirectionalLight", LOCAL) };
    lightNode->Translate(Vector3::UP);
    lightNode->SetDirection(Vector3{ -0.5f, -1.0f, 0.0f });
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetBrightness(.7f);
    light->SetColor(Color{ 1.0f, 0.8f, 0.7f });
    light->SetCastShadows(true);
    light->SetShadowIntensity(0.5f);
    light->SetSpecularIntensity(0.5f);
    light->SetShadowBias(BiasParameters{ 0.0000025f, 2.0f });

    cameraNode_ = overlayScene_->CreateChild("Camera");
    cameraNode_->Translate(Vector3::BACK);
    cameraNode_->LookAt(tableNode_->GetWorldPosition() + Vector3::UP * .23f);
    Camera* camera{ cameraNode_->CreateComponent<Camera>() };

    SharedPtr<Viewport> viewport{ new Viewport{ context_, overlayScene_, camera } };
    SharedPtr<RenderPath> renderPath{ viewport->GetRenderPath()->Clone() };
    renderPath->Load(RES(XMLFile, "RenderPaths/ForwardNoClear.xml"));
    renderPath->Append(RES(XMLFile, "PostProcess/FXAA3.xml"));
    renderPath->SetEnabled("FXAA3", GetSubsystem<Settings>()->GetAntiAliasing());
    renderPath->Append(RES(XMLFile, "PostProcess/BloomHDR.xml"));
    renderPath->SetShaderParameter("BloomHDRThreshold", 0.42f);
    renderPath->SetShaderParameter("BloomHDRMix", Vector2{ 0.75f, 0.5f });
    renderPath->SetEnabled("BloomHDR", GetSubsystem<Settings>()->GetBloom());
    viewport->SetRenderPath(renderPath);

    GetSubsystem<Renderer>()->SetViewport(1, viewport);
}

void Overlay::HideMain()
{
    tome_->Close();

    SharedPtr<ValueAnimation> camRotAnim{ MakeShared<ValueAnimation>(context_) };
    camRotAnim->SetKeyFrame(0.f, Variant{ cameraNode_->GetRotation() });
    Quaternion targetRot{};
    targetRot.FromLookRotation(Vector3::FORWARD);
    camRotAnim->SetKeyFrame(2.f, Variant{ targetRot });
    cameraNode_->SetAttributeAnimation("Rotation", camRotAnim, WM_CLAMP);
}
