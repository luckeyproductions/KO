/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TOME_H
#define TOME_H

#include "Dry/UI/CheckBox.h"

#include "../mastercontrol.h"


#define FONT_CHAOSTIMES RES(Font, "Fonts/ChaosTimes.ttf")

using Bifolio = Pair<SharedPtr<UIElement>, SharedPtr<UIElement>>;

class Tome: public Component
{
    DRY_OBJECT(Tome, Component);

public:
    Tome(Context* context);
    void OnSetEnabled() override;

    bool Save(Serializer& dest) const override;
    bool SaveXML(XMLElement& dest) const override;
    bool SaveJSON(JSONValue& dest) const override;
    void MarkNetworkUpdate() override;
    void GetDependencyNodes(PODVector<Node*>& dest) override;
    void DrawDebugGeometry(DebugRenderer* debug, bool depthTest) override;

    void Open();

    void Close();

protected:
    void OnNodeSet(Node* node) override;
    void OnSceneSet(Scene* scene) override;
    void OnMarkedDirty(Node* node) override;
    void OnNodeSetEnabled(Node* node) override;

private:
    Button* CreateWideButton(String buttonText);
    Button* CreateArrowButton(bool right = false);
    UIElement* CreateLabeledCheckBox(String label);
    IntVector2 PageSize() const { return { 1024, 1280 }; }

    void UpdateFolia(float progression);
    void CreateFolia();
    void SetPage(StringHash page);

    void CreatePages();
    SharedPtr<UIElement> MakePage();
    SharedPtr<UIElement> CreateTitlePage();
    SharedPtr<UIElement> CreateMainMenuPage();
    SharedPtr<UIElement> CreateSettingsSinister();
    SharedPtr<UIElement> CreateSettingsDexter();

    void ConnectButtons();

    void HandlePostUpdate(StringHash eventType, VariantMap& eventData);
    void HandleNewButtonClicked(StringHash, VariantMap&);
    void HandleLoadButtonClicked(StringHash, VariantMap&);
    void HandleSettingsButtonClicked(StringHash, VariantMap&);
    void HandleExitButtonClicked(StringHash eventType, VariantMap& eventData);

    void HandleApplySettingsButtonClicked(StringHash, VariantMap&);
    void HandleMusicCheckBoxClicked(StringHash, VariantMap&);

    void HandleBackArrowClicked(StringHash, VariantMap&);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);

    bool open_;
    bool turned_;
    bool exiting_;
    AnimatedModel* book_;
    Node* sinistrofolio_;
    Node* dextrofolio_;
    UIElement* sinisterRoot_;
    UIElement* dexterRoot_;
    AnimationController* animControl_;

    SharedPtr<Button> backArrow_;
    HashMap<StringHash, Button*> mainMenuButtons_;
    HashMap<StringHash, CheckBox*> settingsBoxes_;
    HashMap<StringHash, Bifolio> bifolia_;
};

#endif // TOME_H
