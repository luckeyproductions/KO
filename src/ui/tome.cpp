/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/UI/UIComponent.h"
#include "../settings.h"
#include "../game.h"

#include "tome.h"


Tome::Tome(Context* context): Component(context),
    open_{ false },
    turned_{ false },
    exiting_{ false },
    book_{ nullptr },
    sinistrofolio_{ nullptr },
    dextrofolio_{ nullptr },
    sinisterRoot_{ nullptr },
    dexterRoot_{ nullptr },
    animControl_{ nullptr },
    backArrow_{ nullptr },
    mainMenuButtons_{},
    bifolia_{}
{
}

void Tome::OnSetEnabled() { Component::OnSetEnabled(); }

void Tome::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetScale(.17f);
    book_ = node_->CreateComponent<AnimatedModel>();
    book_->SetCastShadows(true);
    book_->SetModel(RES(Model, "Models/Tome.mdl"));
    book_->ApplyMaterialList();

    CreateFolia();

    animControl_ = node_->CreateComponent<AnimationController>();
    animControl_->Play("Animations/TomeOpen.ani", 0, false);
    animControl_->SetSpeed("Animations/TomeOpen.ani", 0.f);

    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(Tome, HandlePostUpdate));
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(Tome, HandleKeyDown));
}

void Tome::CreateFolia()
{
    sinistrofolio_ = node_->GetChild("Front", true)->CreateChild("Sinistrofolio");
    dextrofolio_   = node_->GetChild("Back", true) ->CreateChild("Dextrofolio");

    for (Node* folio: { sinistrofolio_, dextrofolio_ })
    {
        const bool sinister{ folio == sinistrofolio_ };

        folio->Roll(90.f - 180.f * sinister);
        folio->Pitch(90.f);
        folio->Scale({ 1.7f, 1.f, 2.3f });
        StaticModel* folioModel{ folio->CreateComponent<StaticModel>() };
        folioModel->SetModel(RES(Model, "Models/Plane.mdl"));

        UIComponent* uiComponent{ folio->CreateComponent<UIComponent>() };
        uiComponent->GetMaterial()->SetTechnique(0, RES(Technique, "Techniques/DiffUnlitAlpha.xml"));
        uiComponent->GetRoot()->SetSize(PageSize());

        if (sinister)
            sinisterRoot_ = uiComponent->GetRoot();
        else
            dexterRoot_   = uiComponent->GetRoot();
    }

    CreatePages();

    SetPage("Main");
    ConnectButtons();

    UpdateFolia(1.f);
}

void Tome::SetPage(StringHash page)
{
    if (!bifolia_.Contains(page))
        return;

    const Bifolio& bifolio{ bifolia_[page] };

    sinisterRoot_->RemoveAllChildren();
    sinisterRoot_->AddChild(bifolio.first_);

    dexterRoot_->RemoveAllChildren();
    dexterRoot_->AddChild(bifolio.second_);

    for (unsigned p{ 0 }; p < bifolia_.Size(); ++p)
    {
        if (bifolia_.Keys().At(p) == page)
            UpdateFolia(-1.f + 2.f * p / bifolia_.Size());
    }

    if (page != "Main")
    {
        turned_ = true;
        sinisterRoot_->AddChild(backArrow_);
    }

    if (page == "Settings")
    {
        Settings* settings{ GetSubsystem<Settings>() };

        settingsBoxes_["Fullscreen"]->SetChecked(settings->GetFullscreen());
        settingsBoxes_["VSync"]->SetChecked(settings->GetVSync());
        settingsBoxes_["AntiAliasing"]->SetChecked(settings->GetAntiAliasing());
        settingsBoxes_["Bloom"]->SetChecked(settings->GetBloom());
        settingsBoxes_["Music"]->SetChecked(settings->GetMusicEnabled());
    }
}

void Tome::ConnectButtons()
{
    SubscribeToEvent(mainMenuButtons_["New Adventure"], E_CLICKEND, DRY_HANDLER(Tome, HandleNewButtonClicked));
    SubscribeToEvent(mainMenuButtons_["Load Game"], E_CLICKEND, DRY_HANDLER(Tome, HandleLoadButtonClicked));
    SubscribeToEvent(mainMenuButtons_["Settings"], E_CLICKEND, DRY_HANDLER(Tome, HandleSettingsButtonClicked));
    SubscribeToEvent(mainMenuButtons_["Exit"], E_CLICKEND, DRY_HANDLER(Tome, HandleExitButtonClicked));
    SubscribeToEvent(backArrow_, E_CLICKEND, DRY_HANDLER(Tome, HandleBackArrowClicked));
}

void Tome::HandlePostUpdate(StringHash, VariantMap&)
{
    const String ani{ "Animations/TomeOpen.ani" };

    if (!open_)
    {
        Input* input{ GetSubsystem<Input>() };

        if (input->GetMouseButtonPress(MOUSEB_ANY))
        {
            Open();
        }
        else for (Key key: { KEY_KP_ENTER, KEY_RETURN, KEY_RETURN2, KEY_SPACE })
        {
            if (input->GetKeyPress(key))
                Open();
        }

        return;
    }
    else if (exiting_ && animControl_->GetTime(ani) == 0.f)
        GetSubsystem<Engine>()->Exit();

    if (!turned_ && !animControl_->IsAtEnd(ani))
    {
        const float aniTime{ animControl_->GetTime(ani) };
        const float aniLength{ animControl_->GetLength(ani) };
        const float progression{ 1.f - 2.f * Pow(aniTime / aniLength, .8f) };

        UpdateFolia(progression);
    }
}

void Tome::Open()
{
    animControl_->SetSpeed("Animations/TomeOpen.ani", 1.f);

    open_ = true;
}

void Tome::Close()
{
    animControl_->SetSpeed("Animations/TomeOpen.ani", -2.f);

    GetSubsystem<UI>()->GetCursor()->SetVisible(false);
}

void Tome::UpdateFolia(float progression)
{
    for (Node* folio: { sinistrofolio_, dextrofolio_ })
    {
        const bool sinister{ folio == sinistrofolio_ };

        book_->SetMorphWeight(StringHash{ "At" }, progression);
        folio->SetPosition({ 0.f, .95f, .21f + progression * (sinister ? .15f : -.15f)});
    }
}

void Tome::HandleNewButtonClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetPage("New");

    GetSubsystem<Game>()->StartNew();
}

void Tome::HandleLoadButtonClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetPage("Load");
}

void Tome::HandleSettingsButtonClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetPage("Settings");
}

void Tome::HandleExitButtonClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Close();
    exiting_ = true;

    for (Button* b: mainMenuButtons_.Values())
        b->UnsubscribeFromAllEvents();
}

void Tome::HandleBackArrowClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetPage("Main");
}

void Tome::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    if (eventData[KeyDown::P_KEY].GetInt() == KEY_ESCAPE)
        SetPage("Main");
}

void Tome::HandleApplySettingsButtonClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Settings* settings{ GetSubsystem<Settings>() };

    if (settingsBoxes_["Fullscreen"]->IsChecked() != settings->GetFullscreen())
        settings->ToggleFullscreen();
    if (settingsBoxes_["VSync"]->IsChecked() != settings->GetVSync())
        settings->ToggleVSync();
    if (settingsBoxes_["AntiAliasing"]->IsChecked() != settings->GetAntiAliasing())
        settings->ToggleAntiAliasing();
    if (settingsBoxes_["Bloom"]->IsChecked() != settings->GetBloom())
        settings->ToggleBloom();
}

void Tome::HandleMusicCheckBoxClicked(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    Settings* settings{ GetSubsystem<Settings>() };

    settings->SetMusicEnabled(settingsBoxes_["Music"]->IsChecked());
}

void Tome::OnSceneSet(Scene* scene) { Component::OnSceneSet(scene); }
void Tome::OnMarkedDirty(Node* node) {}
void Tome::OnNodeSetEnabled(Node* node) {}
bool Tome::Save(Serializer& dest) const { return Component::Save(dest); }
bool Tome::SaveXML(XMLElement& dest) const { return Component::SaveXML(dest); }
bool Tome::SaveJSON(JSONValue& dest) const { return Component::SaveJSON(dest); }
void Tome::MarkNetworkUpdate() { Component::MarkNetworkUpdate(); }
void Tome::GetDependencyNodes(PODVector<Node*>& dest) {}
void Tome::DrawDebugGeometry(DebugRenderer* debug, bool depthTest) {}
