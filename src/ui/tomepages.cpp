/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/UI/Sprite.h"

#include "tome.h"

void Tome::CreatePages()
{
    bifolia_.Insert({"Main", { CreateTitlePage(), CreateMainMenuPage() }});
    bifolia_.Insert({"Settings", { CreateSettingsSinister(), CreateSettingsDexter() }});

    backArrow_ = CreateArrowButton(false);
    backArrow_->SetPosition(-PageSize() / 2 + IntVector2{ 128, 112 });
}

SharedPtr<UIElement> Tome::MakePage()
{
    SharedPtr<UIElement> page{ MakeShared<UIElement>(context_) };
    page->SetAlignment(HA_CENTER, VA_TOP);

    return page;
}

Button* Tome::CreateWideButton(String buttonText)
{
    Button* button{ new Button{ context_ } };
    button->SetFocusMode(FM_RESETFOCUS);
    button->SetSize(960, 192);
    button->SetTexture(RES(Texture2D, "UI/WideButton.png"));
    button->SetImageRect({ 0, 0, 512, 128 });
    button->SetBorder({ 68, 42, 68, 42 });
    button->SetHoverOffset(0, 128);
    button->SetPressedOffset(0, 0);
    button->SetAlignment(HA_CENTER, VA_CENTER);
    button->SetBlendMode(BLEND_ALPHA);
    button->SetOpacity(.9f);

    Text* text{ button->CreateChild<Text>() };
    text->SetAlignment(HA_CENTER, VA_CENTER);
    text->SetPosition(0, 16);
    text->SetText(buttonText);
    text->SetFont(FONT_CHAOSTIMES);
    text->SetColor(Color::BLACK);
    text->SetFontSize(88.f);
    text->SetOpacity(.9f);

    return button;
}

Button* Tome::CreateArrowButton(bool right)
{
    Button* arrowButton{ new Button{ context_ } };
    arrowButton->SetFocusMode(FM_RESETFOCUS);
    arrowButton->SetSize(160, 160);
    arrowButton->SetTexture(RES(Texture2D, String{ "UI/Arrow" } + (right ? "Right" : "Left") + ".png"));
    arrowButton->SetImageRect({ 0, 0, 128, 128 });
    arrowButton->SetHoverOffset(0, 128);
    arrowButton->SetPressedOffset(0, 0);
    arrowButton->SetAlignment(HA_CENTER, VA_CENTER);
    arrowButton->SetBlendMode(BLEND_ALPHA);
    arrowButton->SetOpacity(.9f);

    return arrowButton;
}

UIElement* Tome::CreateLabeledCheckBox(String label)
{
    UIElement* row{ new UIElement{ context_ } };
    row->SetAlignment(HA_LEFT, VA_CENTER);

    Text* text{ row->CreateChild<Text>() };
    text->SetName(label + "Label");
    text->SetAlignment(HA_LEFT, VA_CENTER);
    text->SetPosition(0, 16);
    text->SetText(label);
    text->SetFont(FONT_CHAOSTIMES);
    text->SetColor(Color::BLACK);
    text->SetFontSize(64.f);
    text->SetOpacity(.9f);
    text->SetPosition(0, 0);

    CheckBox* checkBox{ row->CreateChild<CheckBox>() };
    checkBox->SetName(label);
    checkBox->SetFocusMode(FM_RESETFOCUS);
    checkBox->SetSize(128, 128);
    checkBox->SetTexture(RES(Texture2D, "UI/CheckBox.png"));
    checkBox->SetImageRect({ 0, 0, 128, 128 });
    checkBox->SetCheckedOffset(0, 128);
    checkBox->SetAlignment(HA_RIGHT, VA_CENTER);
    checkBox->SetBlendMode(BLEND_ALPHA);
    checkBox->SetOpacity(.9f);
    checkBox->SetPosition(PageSize().x_ - 64, 0);

    return row;
}

SharedPtr<UIElement> Tome::CreateTitlePage()
{
    SharedPtr<UIElement> titlePage{ MakePage() };

    Text* titleA{ titlePage->CreateChild<Text>() };
    titleA->SetText("KO");
    titleA->SetTextAlignment(HA_CENTER);
    titleA->SetFont(FONT_CHAOSTIMES);
    titleA->SetFontSize(101.f);
    titleA->SetColor(Color::BLACK.Transparent(.9f));
    titleA->SetAlignment(HA_CENTER, VA_TOP);
    titleA->SetPosition(0, 42);
    Text* titleB{ titlePage->CreateChild<Text>() };
    titleB->SetText("&\nthe Curse of Greyface");
    titleB->SetTextAlignment(HA_CENTER);
    titleB->SetFont(FONT_CHAOSTIMES);
    titleB->SetFontSize(64.f);
    titleB->SetColor(Color::BLACK.Transparent(.9f));
    titleB->SetAlignment(HA_CENTER, VA_TOP);
    titleB->SetPosition(0, 192);

    Sprite* greyface{ titlePage->CreateChild<Sprite>("Logo") };
    greyface->SetBlendMode(BLEND_ALPHA);
    greyface->SetColor(Color::WHITE.Transparent(.7f));
    greyface->SetTexture(RES(Texture2D, "Textures/Tome/Greyface.png"));
    greyface->SetAlignment(HA_CENTER, VA_CENTER);
    const IntVector2 faceSize{ 820, 512 };
    greyface->SetSize(faceSize);
    greyface->SetPosition(-faceSize.x_ / 2, PageSize().y_ - 3 * faceSize.y_ / 2);

    return titlePage;
}

SharedPtr<UIElement> Tome::CreateMainMenuPage()
{
    SharedPtr<UIElement> mainMenu{ MakePage() };

    for (String buttonText: { "New Adventure", "Load Game", "Settings", "Exit" })
    {
        Button* button{ CreateWideButton(buttonText) };
        button->SetPosition(0, 240 + 224 * mainMenuButtons_.Size());
        mainMenuButtons_.Insert({ buttonText, button });
        mainMenu->AddChild(button);
    }

    return mainMenu;
}

SharedPtr<UIElement> Tome::CreateSettingsSinister()
{
    SharedPtr<UIElement> settingsSinister{ MakePage() };

    Text* title{ settingsSinister->CreateChild<Text>() };
    title->SetText("Settings");
    title->SetTextAlignment(HA_CENTER);
    title->SetFont(FONT_CHAOSTIMES);
    title->SetFontSize(101.f);
    title->SetColor(Color::BLACK.Transparent(.9f));
    title->SetAlignment(HA_CENTER, VA_TOP);
    title->SetPosition(0, 42);

    Text* graphicsHeading{ settingsSinister->CreateChild<Text>() };
    graphicsHeading->SetText("Graphics");
    graphicsHeading->SetTextAlignment(HA_LEFT);
    graphicsHeading->SetFont(FONT_CHAOSTIMES);
    graphicsHeading->SetFontSize(72.f);
    graphicsHeading->SetColor(Color::BLACK.Transparent(.9f));
    graphicsHeading->SetAlignment(HA_LEFT, VA_TOP);
    graphicsHeading->SetPosition(-480, 256);

    int posY{ 500 };
    for (String settingName: { "Fullscreen", "VSync", "AntiAliasing", "Bloom" })
    {
        UIElement* settingRow{ CreateLabeledCheckBox(settingName) };
        settingsSinister->AddChild(settingRow);
        settingRow->SetPosition(-PageSize().x_ / 2 + 64, posY);
        posY += 160;

        settingsBoxes_.Insert({ settingName, static_cast<CheckBox*>(settingRow->GetChild(settingName)) });
    }

    Button* applyButton{ CreateWideButton("Apply") };
    applyButton->SetWidth(640);
    applyButton->SetPosition(34, posY + 23);
    settingsSinister->AddChild(applyButton);

    SubscribeToEvent(applyButton, E_CLICKEND, DRY_HANDLER(Tome, HandleApplySettingsButtonClicked));

    return settingsSinister;
}

SharedPtr<UIElement> Tome::CreateSettingsDexter()
{
    SharedPtr<UIElement> settingsDexter{ MakePage() };

    Text* audioHeading{ settingsDexter->CreateChild<Text>() };
    audioHeading->SetText("Audio");
    audioHeading->SetTextAlignment(HA_LEFT);
    audioHeading->SetFont(FONT_CHAOSTIMES);
    audioHeading->SetFontSize(64.f);
    audioHeading->SetColor(Color::BLACK.Transparent(.9f));
    audioHeading->SetAlignment(HA_LEFT, VA_TOP);
    audioHeading->SetPosition(-480, 256);

    UIElement* settingRow{ CreateLabeledCheckBox("Music") };
    settingsDexter->AddChild(settingRow);
    settingRow->SetPosition(-PageSize().x_ / 2 + 64, 500);

    CheckBox* musicCheckBox{ static_cast<CheckBox*>(settingRow->GetChild(String{ "Music" })) };
    settingsBoxes_.Insert({ "Music", musicCheckBox });
    SubscribeToEvent(musicCheckBox, E_CLICKEND, DRY_HANDLER(Tome, HandleMusicCheckBoxClicked));

    return settingsDexter;
}
