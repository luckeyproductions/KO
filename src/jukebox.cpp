
#include "jukebox.h"

JukeBox::JukeBox(Context* context): Object(context),
    tracks_{},
    musicNode_{ nullptr }
{
    Scene* musicScene{ new Scene(context_) };
    musicNode_ = musicScene->CreateChild("Music");

    SubscribeToEvent(E_MUSICFADED, DRY_HANDLER(JukeBox, HandleMusicFaded));
}

void JukeBox::Play(String song, bool loop)
{
    for (SoundSource* source: tracks_)
    {
        float fadeTime{ .55f };

        SharedPtr<ValueAnimation> fade{ MakeShared<ValueAnimation>(context_) };
        fade->SetKeyFrame(0.f, { source->GetGain() });
        fade->SetKeyFrame(fadeTime, { 0.f });
        fade->SetEventFrame(fadeTime, E_MUSICFADED, {});
        source->SetAttributeAnimation("Gain", fade, WM_ONCE);
    }

    if (!song.IsEmpty())
    {
        Sound* music{ RES(Sound, String{ "Music/" } + song) };
        music->SetLooped(loop);
        SoundSource* musicSource{ musicNode_->CreateComponent<SoundSource>() };
        musicSource->SetSoundType(SOUND_MUSIC);
        musicSource->Play(music);
        tracks_.Push(musicSource);
    }
}

void JukeBox::HandleMusicFaded(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    static_cast<SoundSource*>(GetEventSender())->Remove();
}
