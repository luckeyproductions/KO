/* KO
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include "mastercontrol.h"


class Settings: public Object
{
    DRY_OBJECT(Settings, Object);

public:
    Settings(Context* context);

    bool LoadSettings();
    void SaveSettings();

    int GetWindowWidth() const { return windowWidth_; }
    int GetWindowHeight() const { return windowHeight_; }
    bool GetFullscreen() const { return fullscreen_; }
    bool GetAntiAliasing() const noexcept { return antiAliasing_; }
    bool GetVSync() const { return vsync_; }
    bool GetBloom() const { return bloom_; }

    void ToggleFullscreen();
    void ToggleAntiAliasing();
    void ToggleVSync();
    void ToggleBloom();


    void SetMusicEnabled(bool enabled);
    void SetMusicGain(float gain);

    bool GetMusicEnabled() const { return musicEnabled_; }
    float GetMusicGain() const { return musicGain_; }

private:
    int windowWidth_;
    int windowHeight_;
    bool fullscreen_;
    bool vsync_;
    bool antiAliasing_;
    bool bloom_;
    bool musicEnabled_;
    float musicGain_;
};

#endif // SETTINGS_H
