![Greyface poster](Docs/BIP.png)
# KO and the Curse of Greyface

[![pipeline status](https://gitlab.com/luckeyproductions/ko/badges/master/pipeline.svg)](https://gitlab.com/luckeyproductions/ko/-/commits/master) [![coverage report](https://gitlab.com/luckeyproductions/ko/badges/master/coverage.svg)](https://gitlab.com/luckeyproductions/ko/-/commits/master)

### Summary
Storm the Black Iron Prison, trip your balls inside-out and lift the Curse of Greyface.

![OR Banner](Docs/ORBanner.png){width=200px}![Greyface poster](Docs/GreyfacePoster.png)![OR Banner](Docs/ORBanner.png){width=200px}

### Controls
Dual analog game controller or WASD.

<!--
#### Compiling from source

You may try compiling by running this line in a terminal:

```
git clone https://gitlab.com/LucKeyProductions/Games/KO; cd KO; ./install.sh; cd ..; rm -rf KO
```
 -->
### Screenshot
![KO facing five eyes](Screenshots/Screenshot_Mon_Jun__6_21_23_32_2016.png)

### Tools
- [Dry](https://dry.luckey.games/)
- [QtCreator](http://wiki.qt.io/Category:Tools::QtCreator)
- [Blender](http://www.blender.org/)
- [Inkscape](http://inkscape.org/)
- [GIMP](http://gimp.org)
- [SuperCollider](http://supercollider.github.io/)
- [Audacity](http://web.audacityteam.org/)
